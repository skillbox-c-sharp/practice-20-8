﻿using Challenge1.Entities;
using Microsoft.EntityFrameworkCore;

namespace Challenge1.Contexts
{
    public class ContactDataContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public ContactDataContext() => Database.EnsureCreated();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server=(localdb)\\MSSQLLocalDB;Database=ContactsDB;Trusted_Connection=True;"
                );
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Contact>().HasData(
                new Contact
                {
                    Id = 1,
                    FirstName = "Ivan",
                    LastName = "Parshin",
                    SurName = "Sergeevich",
                    PhoneNumber = "+79069096528",
                    Address = "Санкт-Петербург, Ленина, 20",
                    Description = "Телефон устарел"
                },
                new Contact
                {
                    Id = 2,
                    FirstName = "Semen",
                    LastName = "Semenov",
                    SurName = "Semenovich",
                    PhoneNumber = "+79112544575",
                    Address = "Новосибирск, Столетова, 65",
                    Description = "Не отвечает на звонки"
                },
                new Contact
                {
                    Id = 3,
                    FirstName = "Ekaterina",
                    LastName = "Firsanova",
                    SurName = "Yurievna",
                    PhoneNumber = "+79845548785",
                    Address = "Москва, Сретенка, 54",
                    Description = "Звонить после 15:00"
                }
            );
        }
    }
}
