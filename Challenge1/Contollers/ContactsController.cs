﻿using Challenge1.Contexts;
using Challenge1.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Challenge1.Contollers
{
    public class ContactsController : Controller
    {
        static ContactDataContext contactsDb;

        static ContactsController()
        {
            contactsDb = new ContactDataContext();
        }

        public IActionResult Index(int id)
        {
            Contact currentContact = contactsDb.Contacts.First(d => d.Id == id);
            return View(currentContact);
        }

        public IActionResult Delete(int? id) => ContactPage(id);

        [HttpPost]
        public IActionResult Delete(int id)
        {
            Contact? contact = contactsDb.Contacts.Find(id);
            if (contact != null)
            {
                contactsDb.Contacts.Remove(contact);
                contactsDb.SaveChanges();
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Edit(int? id) => ContactPage(id);

        [HttpPost]
        public IActionResult Edit(int id, string lastName, string firstName, string surName, string phoneNumber,
            string address, string description)
        {
            Contact contact = contactsDb.Contacts.First(d => d.Id == id);
            contact.LastName = lastName;
            contact.FirstName = firstName;
            contact.SurName = ValueOrDefault(surName);
            contact.PhoneNumber = phoneNumber;
            contact.Address = address;
            contact.Description = ValueOrDefault(description); 

            contactsDb.Contacts.Update(contact);
            contactsDb.SaveChanges();

            return RedirectToAction("Index", new { id = id });
        }

        private IActionResult ContactPage(int? id)
        {
            if (id != null)
            {
                Contact? contact = contactsDb.Contacts.FirstOrDefault(p => p.Id == id);
                if (contact != null) return RedirectToAction("Index", new { id = id });
            }
            return NotFound();
        }

        private string ValueOrDefault(string value)
        {
            if (!string.IsNullOrEmpty(value)) return value;
            else return "";
        }
    }
}
