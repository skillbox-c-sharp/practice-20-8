﻿using Challenge1.Contexts;
using Challenge1.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Challenge1.Contollers
{
    public class HomeController : Controller
    {
        static private ContactDataContext contactDb;

        static HomeController() => contactDb = new ContactDataContext();
        public IActionResult Index()
        {
            ViewBag.Contacts = contactDb.Contacts;
            return View();
        }

        [HttpPost]
        public IActionResult Add(string lastName, string firstName, string surName, string phoneNumber,
            string address, string description)
        {
            if (string.IsNullOrEmpty(surName)) surName = "";
            if (string.IsNullOrEmpty(description)) description = "";
            Contact contact = new Contact
            {
                LastName = lastName,
                FirstName = firstName,
                SurName = surName,
                PhoneNumber = phoneNumber,
                Address = address,
                Description = description
            };

            contactDb.Contacts.Add(contact);
            contactDb.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
